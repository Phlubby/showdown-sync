﻿namespace Tic80ShowdownClientServer
{
    using System;
    using System.IO;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;

    public class ClientUDP
    {
        // TODO
        // https://www.codeproject.com/Questions/72657/Send-UDP-packet-in-C
        // or
        // https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.unixdomainsocketendpoint?view=netcore-2.1

        private string _serverIp = string.Empty;
        private int _port = -1;
        private UdpClient _udpClient = null;
        private Datagram _datagram = new Datagram();

        public string ShowdownId = string.Empty;
        public string Filename = string.Empty;

        public ClientUDP()
        {
        }

        public int Init(string serverIp, int port)
        {
            if (_serverIp == serverIp || _port == port) { return 0; }

            try
            {
                this._serverIp = serverIp;
                this._port = port;
                if (_udpClient != null)
                {
                    _udpClient.Dispose();
                }
                _udpClient = new UdpClient(serverIp, port);
                return 0;
            }
            catch(SocketException se)
            {
                return se.NativeErrorCode;
            }
        }

        public void Send(string data)
        {
            Console.Write("SENDING> {0}, {1}, {2}", _datagram.ShowdownId, _datagram.FileName, _datagram.FileContent.Length);

            byte[] sendBytes = Encoding.ASCII.GetBytes(data);
            if (_udpClient != null)
            {
                _udpClient.Send(sendBytes, sendBytes.Length);
            }
            Console.WriteLine(" <DONE");
        }

        public void Run()
        {
            Console.WriteLine("\nRunning as a client to server on {0}:{1}...", _serverIp, _port);
            Console.WriteLine("Pressing ESC key ends the wole thing immediately");

            _datagram = new Datagram();
            _datagram.ShowdownId = this.ShowdownId;
            _datagram.FileName = this.Filename;
            Datagram.FromFile(_datagram);

            Console.WriteLine("\nWatching file {0}", Datagram.GetFullFileName(_datagram.FileName));

            this.Send(Datagram.ToString(_datagram));

            var fw = new FileSystemWatcher
            {
                Filter = Filename,
                Path = System.IO.Path.GetDirectoryName(Datagram.GetFullFileName(_datagram.FileName)),
                IncludeSubdirectories = false,
                SynchronizingObject = null,
                EnableRaisingEvents = true,
                NotifyFilter = NotifyFilters.LastWrite
            };

            // On .NET Core 3.0 MonikerChange runs in a different Thread  
            fw.Changed += MonikerChanged;
            // On .NET Framework 4.x, 3.x etc. It runs in the same Thread  

            bool keepGoing = true;
            while (keepGoing)
            {
                if (Console.ReadKey().Key == ConsoleKey.Escape)
                {
                    keepGoing = false;
                }
            }

            Console.WriteLine("\nSession end. Closing.");
            _udpClient.Close();
            _udpClient.Dispose();
        }

        private void MonikerChanged(object sender, FileSystemEventArgs e)
        {
            //for editors that first wipe the whole file and then add the current contents
            // happens on windows with various editors
            Thread.Sleep(50);
            Datagram.FromFile(_datagram);
            this.Send(Datagram.ToString(_datagram));
        }

    }
}
