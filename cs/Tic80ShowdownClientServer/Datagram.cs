﻿namespace Tic80ShowdownClientServer
{
    //usings inside namespace because 'reasons'
    using System;
    using System.IO;
    using System.Text;

    public enum DatagramFields
    {
        ShowdownId = 0,
        FileName = 1,
        FileContent = 2
    }

    public class Datagram
    {
        public static string Splitter = "|";

        //using getters/setter since there was a plan for a GUI version, databinding is easier with getters/setters
        public string ShowdownId { get; set; }
        public string FileName { get; set; }
        public string FileContent { get; set; }

        public static string GetFullFileName(string filename)
        {
            if (System.IO.Path.IsPathFullyQualified(filename)) { return filename; }
            else
            {
                //_fullPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                //_fullPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                //_fullPath = Application.StartupPath;
                //_fullPath = System.IO.Directory.GetCurrentDirectory();
                //_fullPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                return System.IO.Path.Combine(Environment.CurrentDirectory, filename);
                //_fullPath = System.IO.Path.GetDirectory(Application.ExecutablePath);
                //return System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filename);
            }
        }

        public static string ToString(Datagram datagram)
        {
            string s = datagram.ShowdownId + Splitter + datagram.FileName + Splitter + datagram.FileContent;
            return s;
        }

        public static bool FromString(string data, Datagram datagram)
        {
            try
            {
                int nrItems = Enum.GetValues(typeof(DatagramFields)).Length;
                var items = data.Split(Splitter, nrItems);
                datagram.ShowdownId = items[(int)DatagramFields.ShowdownId];
                datagram.FileName = items[(int)DatagramFields.FileName];
                datagram.FileContent = items[(int)DatagramFields.FileContent];
                return true;
            }
            catch
            {
                return false;
            }
           
        }

        public static bool ToFile(Datagram datagram)
        {
            try
            {
                File.WriteAllText(Datagram.GetFullFileName(datagram.FileName), datagram.FileContent, Encoding.UTF8);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool FromFile(Datagram datagram)
        {
            try
            {
                datagram.FileContent = File.ReadAllText(Datagram.GetFullFileName(datagram.FileName), Encoding.UTF8);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
