﻿namespace Tic80ShowdownClientServer
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;

    public class ServerUDP
    {
        // TODO
        // https://www.codeproject.com/Questions/72657/Send-UDP-packet-in-C
        // or
        // https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.unixdomainsocketendpoint?view=netcore-2.1

        private int _port = -1;
        private Socket _socket = null;

        public ServerUDP() { }

        public int Init(int port)
        {
            if (_port == port)
            {
                return 0;
            }

            try
            {
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                _socket.Bind(new IPEndPoint(IPAddress.Any, port));
                this._port = port;
                return 0;
            }
            catch (SocketException se)
            {
                return se.NativeErrorCode;
            }
        }

        public void Run()
        {
            if (_socket.IsBound)
            {
                Console.WriteLine("Running on port: {0}...", this._port);

                //64k buffer, about te max for udp packetsize
                byte[] receiveBytes = new byte[64 * 1024];
                while (true)
                {
                    Console.WriteLine("waiting...");
                    int size = _socket.Receive(receiveBytes);
                    string returnData = Encoding.ASCII.GetString(receiveBytes, 0, size);
                    HandleOutput(returnData);
                }
            }

            Console.WriteLine("Session end. Closing.");
            _socket.Close();
            _socket.Dispose();
        }

        public void HandleOutput(string data)
        {
            var newDatagram = new Datagram();

            if (!Datagram.FromString(data, newDatagram))
            {
                Console.WriteLine("faulty data received: could not convert");
            }

            if (!Datagram.ToFile(newDatagram))
            {
                Console.WriteLine("faulty data received: could not save");
            }

            Console.WriteLine("{0}, {1}, {2}", newDatagram.ShowdownId, newDatagram.FileName, newDatagram.FileContent.Length);
        }
    }
}
