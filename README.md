# README #

The CS and Python3 folders are the simplistic 'file synchronisers over UDP protocol' for the TIC-80 showdown of the LoveByte democompo

CS: dotNETCore implementation, client server are a singular appication.

python3: python3 implementation, Client and Server are separate.

The ticzip is a python3 (re)implementation of Gargaj's TIC-80 cart builder (with compression support), but extended with commandline params, default settings and whatnot.
It also extends to the newest TIC-80 version(s) (+v0.90.1552) concerning the use of the new palette, instead of the old palette.

VERY IMPORTANT!
* This tic-packer was specifically made for sizecoding competitions.
* assumption is that the code (compressed or uncompressed) fits inside 1 chunk
* no handling of code that is larger than 1 chunk (64k / 65535bytes) is implemented

The CS project is developed with Visual Studio 2019 Comunity edition, but the simplistic approach and little amount of files should make it easily portable to any environment.

The python3 should run almost out of the box for every environment you set up (pycharm, notepad, etc) as long as you have python3.

### What is this repository for? ###

* The Showdown sync is a tool to sync files over UDP, mainly developed for the upcoming LoveByte democompo, and its TIC-80 Showdown (tryout for a TIC-80 version of a Shader Showdown competition)
* The ticzip is an exended version to build a TIC-80 cart from a lua source file, with options to (gzip)compress the source, and add the fairly new DefaultChunk that 'activates' the new (better looking) palette when loading a cart.

### How do I get set up? ###

* Visual Studio 2019 Community (for CS project)
* python3 for python projects

### Who do I talk to? ###

* ehm... me... not really fond of posting my email though
* the LoveByte crew